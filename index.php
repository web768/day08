<?php
session_start();
$gender = array("Nam", "Nữ");
$department = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");

?>
<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">

    <!-- base style  -->
    <link rel="stylesheet" href="styles.css">

    <!-- bootstrap -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <title>All Student</title>
</head>

<body>
<body>
    <div class="all-student">
        <div class="search-box">
            <form id="search-form" action="" method="get">
                <div class="input-box">
                <label for="department" class="text-label">Phân khoa</label>
                    <select name="department" id="department" class="select-field">
                        <option value="" disabled selected value>Chọn phân khoa</option>
                        <?php
                        foreach ($department as $key => $value) {
                            if ($_GET['department'] == $key) {
                                echo "<option value='$key' selected>$value</option>";
                            } else {
                                echo "<option value='$key'>$value</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="input-box">
                    <label for="" class="text-label">Từ khóa</label>
                    <?php
                        if (isset($_GET['keyword'])) {
                            $keyword = $_GET['keyword'];
                        } else {
                            $keyword = "";
                        }
                        echo "<input type='text' name='keyword' id='keyword' class='text-field' value='$keyword'>";
                    ?>
                </div>
                <div class="d-flex justify-content-center">
                    <div class="p-2">
                        <div class="btn">
                            <input class="btn-submit" type="submit" value="Tìm kiếm"></input>
                        </div>
                    </div>
                    <div class="p-2">
                        <div class="btn">
                            <input class="btn-submit" name="clear" type="button" onclick="clearForm()" value="Xóa"></input>
                        </div>
                    </div>  
                </div>
            </form>
        </div>
        <div class="student-count">
            <div class="student-found">
                <p><b>Số sinh viên tìm thấy: XXX</b></p>
            </div>
            <a href="create.php" class="btn-submit">Thêm</a>
        </div>
        <div class="student-list">
            <table class="student-table">
                <tr class="header">
                    <td style="width:10%"><b>No.</b></td>
                    <td style="width:40%"><b>Tên sinh viên</b></td>
                    <td style="width:40%"><b>Khoa</b></td>
                    <td style="text-align:center; width:10%"><b>Action</b></td>
                </tr>
                <tr class="student-item">
                    <td>1</td>
                    <td style="">Nguyễn Thị Hải Anh</td>
                    <td>Khoa học máy tính</td>
                    <td class="action">
                        <buton class="btn-action">Xóa</buton>
                        <buton class="btn-action">Sửa</buton>
                    </td>
                </tr>
                <tr class="student-item">
                    <td>2</td>
                    <td style="">Hải Anh Nguyễn Thị</td>
                    <td>Khoa học máy tính</td>
                    <td class="action">
                        <buton class="btn-action" >Xóa</buton>
                        <buton class="btn-action">Sửa</buton>
                    </td>
                </tr>
                <tr class="student-item">
                    <td>3</td>
                    <td style="">Thị Nguyễn Hải Anh</td>
                    <td>Khoa học dữ liệu</td>
                    <td class="action">
                        <buton class="btn-action">Xóa</buton>
                        <buton class="btn-action">Sửa</buton>
                    </td>
                </tr>
                <tr class="student-item">
                    <td>4</td>
                    <td style="">Anh Nguyễn Thị Hải</td>
                    <td>Khoa học dữ liệu</td>
                    <td class="action">
                        <buton class="btn-action">Xóa</buton>
                        <buton class="btn-action">Sửa</buton>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>

<script>
    function clearForm() {
        // document.getElementById("search-form").reset();
        document.getElementById("department").value = "";
        document.getElementById("keyword").value = "";
    }
</script>


</html>

